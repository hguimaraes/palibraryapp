## paLibraryApp

### Description

WebApp for the "paLibrary" Backend. Also uses the SSL certificates and Keys for security.
The bakend can found here: https://github.com/Hguimaraes/paLibrary

### Implemented

1. SignUp
2. SignIn
3. Upload a new Book
4. Search for Books
5. Download a Book

### How to run
```shell-script
	npm install bower -g
	npm install && bower install
	node server
```

And just open your browser at: localhost:3000

### SSL

**Key**:
```shell-script
	openssl req -newkey rsa:2048 -new -nodes -keyout key.pem -out csr.pem
	openssl x509 -req -days 365 -in csr.pem -signkey key.pem -out server.crt
```

password: ouwsuchpass123
 
### References
1. http://stackoverflow.com/questions/11744975/enabling-https-on-express-js
2. http://stackoverflow.com/questions/22584268/node-js-https-pem-error-routinespem-read-biono-start-line