/**
 * => Create a simple server to handle the WebApp.
 * All the requests and other stuff comming from a different app
 * (The backend which is totally uncoupled) 
 */

// modules ==========================================================
var express         = require('express');
var bodyParser      = require('body-parser');
var morgan          = require('morgan'); 		// used to see requests
var path            = require('path');
var cookieParser    = require('cookie-parser');
var session         = require('express-session');
var fs              = require('fs');
var https           = require('https');

var config 			= require('./config/config');
var app 			= express();

var public_folder   = 'public';

// Configuration ====================================================

// SSL
var privateKey  = fs.readFileSync(config.server.key, 'utf8');
var certificate = fs.readFileSync(config.server.cert, 'utf8');
var credentials = {
	key: privateKey,
	cert: certificate,
	requestCert: false,
    rejectUnauthorized: false
};

// use body parser so we can grab information from POST requests
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure our app to handle CORS requests
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
	next();
});

// log all requests to the console 
app.use(morgan('dev'));

// Setting port and static path to express
app.use(express.static(path.join(__dirname, public_folder)));

// Send users to front-end
app.get('*', function(req, res) {
	res.sendFile(path.join(__dirname + '/' + public_folder + '/index.html'));
});

// Listen ===========================================================
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(config.port);
console.log("- Express server listening on port " + config.port);