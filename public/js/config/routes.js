angular.module('paLibraryApp')
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/view/signin.html',
        controller: 'SignInCtrl'
      })
      .when('/signup', {
        templateUrl: '/view/signup.html',
        controller: 'SignUpCtrl'
      })
      .when('/bookstore', {
        templateUrl: '/view/home.html',
        controller: 'HomeCtrl'
      }).otherwise({
        redirectTo: "/"
      });
    $locationProvider.html5Mode(true);
  });