angular.module("paLibraryApp")
  .controller("SignInCtrl", function ($scope, $routeParams, $location, $http) {
    $scope.params = $routeParams;
    $scope.signin = {
    	"usuario": "",
    	"senha": ""
    };

    $scope.login = function(){
    	console.log($scope.signin);
    	$http.post(API_ADDRESS + "login", $scope.signin).then(
    		function (response) {
    			console.log(response);
    			if(response.data){
    				$location.path('/bookstore');
    			} else {
    				Materialize.toast('Usuário ou senha incorretos!', 4000);
    			}
    			
    		}, function (response) { 
    			console.log(response);
    			alert("ERROR RESPONSE, CHECK THE CONSOLE");
    		});
    };
});