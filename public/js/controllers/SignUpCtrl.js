angular.module("paLibraryApp")
  .controller("SignUpCtrl", function ($scope, $routeParams, $location, $http) {
    $scope.params = $routeParams;
    $scope.repass = "";
    $scope.signup = {
    	"usuario": "",
    	"senha": ""
    };

    $scope.signupUser = function(){
    	console.log($scope.signup);
    	if($scope.signup.senha == $scope.repass){
	    	$http.post(API_ADDRESS + "signup", $scope.signup).then(
	    		function (response) {
	    			console.log(response);
	    			if(response.data){
	    				$location.path('/bookstore');
	    			} else {
	    				Materialize.toast('Usuário já cadastrado! Tente outro Username', 5000);
	    			}
	    			
	    		}, function (response) { 
	    			console.log(response);
	    			alert("error");
	    	});
    	} else {
    		Materialize.toast('Senhas não batem!', 4000);
    	}
    };
});