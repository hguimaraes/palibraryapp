angular.module("paLibraryApp")
  .controller("HomeCtrl", function ($scope, $routeParams, $location, $http) {
    $scope.params = $routeParams;
    // Data from the Modal form of our application
		$scope.modalForm = {};

		//
		$scope.searchResult = false;
		$scope.searchResultData = null;
		
		$scope.searchForm = {
			titulo: '',
			autoria: '',
			veiculo:'',
			from_dataPublicacao:'',
			to_dataPublicacao:''
		};

		$scope.searchBy = {
			Titulo: true,
			Autoria: false,
			Veiculo: false,
			DateRange: false
		};

		// Function to send data as post to the backend
		$scope.saveData =  function(){
			console.log($scope.modalForm);
			/*$http.post(API_ADDRESS + "saveBook?title=" + $scope.modalForm.titulo +
				"&author=" + $scope.modalForm.autoria +
				"&publisher=" + $scope.modalForm.veiculo +
				"&realeaseDate=" + $scope.convertDate($scope.modalForm.dataPublicacao)
				,$scope.modalForm).success(function(response) {*/
			$http.post(API_ADDRESS + "saveBook", $scope.modalForm).success(function(response) {		
				Materialize.toast("Livro cadastrado com sucesso!");
			});
		};

		// Custom search in the Catalog
		$scope.searchCatalog = function(){
			$scope.searchResult = true;
			var checked = "";

			for(var x in $scope.searchBy){
				if($scope.searchBy[x]){
					checked = x;
					break;
				}
			}

			switch(checked){
				case "Titulo":
					$scope.searchResultData = $scope.searchCatalogTitle($scope.searchForm.titulo);
					break;

				case "Autoria":
					$scope.searchResultData = $scope.searchCatalogAutoria($scope.searchForm.autoria);
					break;

				case "Veiculo":
					$scope.searchResultData = $scope.searchCatalogVeiculo($scope.searchForm.veiculo);
					break;

				case "DateRange":
					$scope.searchResultData = $scope.searchCatalogDateRange(
						$scope.convertDate($scope.searchForm.from_dataPublicacao),
						$scope.convertDate($scope.searchForm.to_dataPublicacao));
					break;

				default:
					$http.get(API_ADDRESS + "books").success(function(response) {
						$scope.searchResultData = response;
					});
					break;

			}
		};

		// Clear the Search Form and results
		$scope.clearSearchForm = function(){
			$scope.searchResultData = null;
			$scope.searchForm = {};
			$scope.searchResult = false;
		};

		// Delete a Book from the Backend
		$scope.deleteBook = function(Book){
			var BookId = Book.patrimonio;
			$http.get("deleteById?q=" + BookId)
				.success(function(response) {
					Materialize.toast("Livro deletado com sucesso!", 4000);
					$scope.clearSearchForm();
			});
		};

		$scope.downloadBook = function(Book){
			var BookId = book.patrimonio;
			$http.get("downloadPDF?q=" + BookId)
				.success(function(response) {
					console.log(response);
			});
		};

		// Function to open the Modal edit form with the values filled
		$scope.editFromSearch = function(Book){
			// Book ID
			$scope.modalForm.patrimonio = Book.patrimonio;
			$scope.modalForm.titulo = Book.titulo;
			$scope.modalForm.autoria = Book.autoria;
			$scope.modalForm.veiculo = Book.veiculo;
			$scope.modalForm.dataPublicacao = new Date(Book.dataPublicacao);
			$('#upload-modal').openModal();
		};

		// Function to clear the values in the form modal
		$scope.clearModalData = function(){
			$scope.modalForm = {};
		};

		// Set all checkboxes to unchecked
		$scope.resetCheckBoxes = function(){
			$scope.searchBy = {
			Titulo: false,
			Autoria: false,
			Veiculo: false,
			DateRange: false,
			Keywords: false
		};
		};

		// Only one checkbox checked at time
		$scope.updateSelection = function(Field){
			var initialState = $scope.searchBy[Field]; 
			$scope.resetCheckBoxes();
			$scope.searchBy[Field] = initialState;
		};

		//
		$scope.searchCatalogTitle = function(title){
			$http.get(API_ADDRESS + "findByTitle?q=" + title).success(function(response) {
				$scope.searchResultData = response;
			});
		};

		//
		$scope.searchCatalogAutoria = function(author){
			$http.get(API_ADDRESS + "findByAuthor?q=" + author).success(function(response) {
				$scope.searchResultData = response;
			});	
		};

		//
		$scope.searchCatalogVeiculo = function(publisher){
			$http.get(API_ADDRESS + "findByPublisher?q=" + publisher).success(function(response) {
				$scope.searchResultData = response;
			})	
		};

		//
		$scope.searchCatalogDateRange = function(date_from, date_to){
			$http.get(API_ADDRESS + "findByDateRange?start=" + date_from + "&end=" + date_to).success(function(response) {
				$scope.searchResultData = response;
			})	
		};

		//
		$scope.convertDate = function(old_date){
			function formatNumber(number) {
				if (number < 10) {
					number =  '0' + number;
				} else {
					number = '' + number;
				}
				return number;
			}

			var dateObj = new Date(old_date);
			var month = formatNumber(dateObj.getUTCMonth() + 1); //months from 1-12
			var day = formatNumber(dateObj.getUTCDate());
			var year = dateObj.getUTCFullYear();

			return year + "-" + month + "-" + day + " 00:00:00";
		};
}).directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);